# docker build -t phpfpm-nginx-test/phpfpm-app:1.0.0 .

#FROM php:7.4-fpm AS phpfpm-app
FROM php:7.4-fpm
#RUN pecl install redis && docker-php-ext-enable redis
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions redis memcached
RUN mkdir /app
COPY hello.php image.jpg /app/
RUN ln -s hello.php /app/index.php
# comment user and group directive as master process is run as user in OpenShift anyhow
RUN sed -i.bak -e 's/^user/;user/' -e 's/^group/;group/' /usr/local/etc/php-fpm.d/www.conf
